# unzipm

I sometimes download music from e.g., Bandcamp in the form of a zip archive with the tile formatted: "[Artist Name] - [Album Title].zip". For my own organization, I want the contents of this zip to go in the directory: [Artist Name]/[Album Title]/. This script is designed to accomplish that.

This utility deletes the original zip after extracting it. If you do not want it to do this, comment out the last line.

## Dependencies

In order to run properly, this script must be able to call `unzip`, as well as basic commands including `cd`, `mkdir`, and `rm`.

## Installing

This script can be installed by running the `install` script as root, or by copying `unzipm` to `/usr/bin/`

To add the manual for `unzipm` (if manually installing), also copy `unzipm.1` to `/usr/local/share/man/man1/`, then run `mandb` as root.

The `install` script does not remove these files after copying them, but it is safe to do so if you wish.

## Usage

`unzipm [OPTIONS] filename.zip`

## Options

`-c CATEGORY` - Specify the album's category. For example, you might sort your music into full-length albums, EPs, demos, and singles. The resulting structure will be [Artist Name]/[CATEGORY]/[Album Title]/

`-y YEAR` - Specify the album's release year. The resulting structure will instead be [Artist Name]/[YEAR] - [Album Title]/

If both `-c` and `-y` are given, the resulting structure will be [Artist Name]/[CATEGORY]/[YEAR] - [Album Title]/
